The note-review app helps you review your notes by the last reviewed date. When you run note-review, it will scan the configured directory and sync the discovered files with the database. Afterwards, it will display a single note that needs review and update the last reviewed date.

## Dependencies
* Go 1.18
* Sqlite 3

## Installation
After cloning, run the following commands:
```
> mv config.yaml.example config.yaml
> sqlite3 notes.db
sqlite> create table note_review(id integer primary key autoincrement, path text, last_reviewed text);
sqlite> .exit
> make
```

Now use your editor of choice to change the values in your config.yaml file.
- `directory`: the absolute path that contains all your note files
- `extension`: the file type of notes you want to review
- `exclusions`: a list of file/path strings that the review app will ignore

Finally, run `./notes`
