package main

import (
	"database/sql"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"github.com/spf13/viper"
	"golang.org/x/exp/slices"
)

type Note struct {
	id            int
	path          string
	last_reviewed string
}

func getExtFiles(dirname string, ext string, exclusions []string) ([]string, error) {
	var orgFiles []string

	err := filepath.WalkDir(dirname, func(s string, d fs.DirEntry, err error) error {
		// skip if file path name is excluded
		for _, exclusion := range exclusions {
			if strings.Contains(s, exclusion) {
				return nil
			}
		}

		if filepath.Ext(d.Name()) == ext {
			orgFiles = append(orgFiles, s)
		}

		return nil
	})

	if err != nil {
		return nil, err
	}

	return orgFiles, nil
}

func errorOut(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func getInsertStatement(orgFiles []string) string {
	insertStmt := "insert into note_review (path, last_reviewed) values"

	for index, file := range orgFiles {
		insertStmt += fmt.Sprintf(" (\"%v\", \"%v\")", file, time.Now())
		if index < len(orgFiles)-1 {
			insertStmt += ","
		}
	}
	return insertStmt
}

func findRowsToDelete(orgFiles *[]string, allScannedRows *[]Note) []int {

	var rowsToDelete []int

	for _, note := range *allScannedRows {
		dbPathMapsToFile := slices.Contains(*orgFiles, note.path)

		if !dbPathMapsToFile {
			rowsToDelete = append(rowsToDelete, note.id)
		}
	}

	return rowsToDelete
}

func findFilesToAdd(orgFiles *[]string, allScannedRows *[]Note) []string {

	var filesToAdd []string
	var rowPaths []string

	// create a slice of paths found in the database so we can easily compare
	for _, scannedRow := range *allScannedRows {
		rowPaths = append(rowPaths, scannedRow.path)
	}

	// find the files that need to be added to the database
	for _, file := range *orgFiles {
		fileIsInDatabase := slices.Contains(rowPaths, file)

		if !fileIsInDatabase {
			filesToAdd = append(filesToAdd, file)
		}
	}

	return filesToAdd
}

func getRows(allRows *sql.Rows) []Note {
	var allNotes []Note

	for allRows.Next() {
		var id int
		var path string
		var last_reviewed string
		err := allRows.Scan(&id, &path, &last_reviewed)
		errorOut(err)
		allNotes = append(allNotes, Note{id: id, path: path, last_reviewed: last_reviewed})
	}

	return allNotes
}

func updateRow(id int, db *sql.DB) error {

	updateStmt := fmt.Sprintf("update note_review set last_reviewed = \"%v\" where id = %v", time.Now(), id)

	_, err := db.Exec(updateStmt)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("yaml")   // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(".")
	err := viper.ReadInConfig() // Find and read the config file
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			errorOut(err)
		} else {
			// Config file was found but another error was produced
			errorOut(err)
		}
	}

	directory := viper.GetString("directory")
	extension := viper.GetString("extension")
	exclusions := viper.GetStringSlice("exclusions")
	orgFiles, err := getExtFiles(directory, extension, exclusions)
	errorOut(err)

	db, err := sql.Open("sqlite3", "./notes.db")
	errorOut(err)
	defer db.Close()

	// query all existing records
	allRows, err := db.Query("select * from note_review order by date(last_reviewed);")
	errorOut(err)
	defer allRows.Close()

	allScannedRows := getRows(allRows)

	rowIdsToDelete := findRowsToDelete(&orgFiles, &allScannedRows)
	filesToAdd := findFilesToAdd(&orgFiles, &allScannedRows)

	// delete records
	numRowsToDelete := len(rowIdsToDelete)
	if numRowsToDelete > 0 {
		deleteStmt := "delete from note_review where id in ("
		for i := 0; i < numRowsToDelete; i += 1 {
			deleteStmt += fmt.Sprintf("%d", rowIdsToDelete[i])
			if i != numRowsToDelete-1 {
				deleteStmt += fmt.Sprintf(", ")
			}
		}

		deleteStmt += fmt.Sprintf(")")

		_, err := db.Exec(deleteStmt)
		errorOut(err)
	}

	// add records
	numFilesToAdd := len(filesToAdd)
	if numFilesToAdd > 0 {
		insertStmt := getInsertStatement(filesToAdd)
		_, err := db.Exec(insertStmt)
		errorOut(err)
	}

	// query for the oldest "last_reviewed" record
	rowToReview, err := db.Query("select * from note_review order by last_reviewed asc limit 1")
	errorOut(err)
	defer rowToReview.Close()

	// set the datetime for that record
	noteToReview := getRows(rowToReview)
	err = updateRow(noteToReview[0].id, db)
	errorOut(err)

	// open that file and retrieve the text
	// print the text of that file
	// exit
	data, err := os.ReadFile(noteToReview[0].path)
	errorOut(err)
	os.Stdout.Write(data)
}
